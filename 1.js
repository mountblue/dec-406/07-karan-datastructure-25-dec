class Node{
    constructor(element){
        this.element = element;
        this.next = null;
    }
}

class LinkedList{
    constructor(){
        this.head = this.head;
        this.size =0;
    }

    print(){
        var curr = this.head;
        var res="";
        while(curr){
            res = res + " "+curr.element;
            curr = curr.next;
        }
        console.log(res);
    }

    //task 1
    task1(){
        var curr = this.head;
        var prev = null;
        var k = this.head.element;

        while(curr!=null){
            if(k>curr.element){
             curr = curr.next;
             prev.next = curr;
            }else{
            prev = curr;
            curr = curr.next;
            k = prev.element;
            }
        }
    }

    //task 2
    task2(m,n){
        var curr = this.head;
        var prev;
        
        while(curr!=null){
            for(var i=1; i<m; i++){
                if(curr.next!=null){
                    curr = curr.next;
                }
            }
            prev = curr;

            for(var i=1; i<=n; i++){
                if(curr.next!=null){
                    curr = curr.next;
                }
            }
            prev.next = curr.next;
            curr = curr.next;
        }
    }

    //task3
    task3(head,k){
        
    }

    //task4
    task4(){
        var curr = this.head;
        var prev = null;
        var next;
        while(curr!=null && curr.next!=null){
            curr = curr.next;
            next = curr.next

            if(prev == null){
                this.head.next = next;
                curr.next = this.head;
                this.head = curr;
            }else{
                prev.next.next = next;
                curr.next = prev.next;
                prev.next = curr;
            }
            prev = curr.next;
            curr = next;
        }   
    }

    //task5
    task5(k){
        var curr = this.head;
        for(var i=0; i<k; i++){
            if(curr!=null){
                curr = curr.next;
            }
        }
        if(curr == null){
            return null;
        }
        return curr.element;
    }
}

var l = new LinkedList();
var node1 = new Node(1);
var node2 = new Node(4);
var node3 = new Node(4);
var node4 = new Node(3);
var node5 = new Node(6);
var node6 = new Node(8);
var node7 = new Node(7);
var node8 = new Node(1);


l.head = node1;
node1.next = node2;
node2.next = node3;
node3.next = node4;
node4.next = node5;
node5.next = node6;
node6.next = node7;
node7.next = node8;

l.print();

